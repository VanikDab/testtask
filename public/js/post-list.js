$(document).ready(function () {

    $(document).on('click', '.load-more', (e)=> {

        e.preventDefault()

        let link  = $(e.target)

        $.ajax({
            url: link.attr('href'),
            type: 'GET',
            success: function (data) {
                let html = $(".content-data")[0].outerHTML
                // let html = $('.append-block').html();
                $('.append-block').html(html + data);
            },
            error: function (data) {
                alert(data);
            }
        })
    })
});
