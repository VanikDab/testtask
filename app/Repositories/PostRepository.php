<?php


namespace App\Repositories;


use App\Interfaces\PostInterface;
use App\Models\Post;


class PostRepository extends BaseRepository implements PostInterFace
{
    /**
     * PostRepository constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    /**
     * @param array $data
     * @return array
     */
    public function store(array $data)
    {
        $data = $this->workSingleImage($data);
        $post = auth()->user()->posts()->create($data);

        return $post ;
    }

    /**
     * @param Post $post
     * @param array $data
     * @return bool|mixed
     */
    public function update(Post $post , array $data)
    {
        $data = $this->workSingleImage($data);
        $post =  $post->update($data);

        return $post ;
    }


    /**
     * @return mixed
     */
    public function userPosts()
    {
        return  auth()->user()->posts;
    }

    /**
     * @param Post $post
     * @return Post|mixed
     */
    public function edit(Post $post)
    {
        return $post->load('comments');
    }
}
