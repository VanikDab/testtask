<?php


namespace App\Repositories;


use App\Interfaces\BaseInterFace;
use App\Services\UploadService;
use Illuminate\Support\Arr;

class BaseRepository implements BaseInterFace
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    protected $relation;

    /**
     * @return mixed
     */
    public function get()
    {
        return  $this->model->simplePaginate(3);
    }

    /**
     * @param array $data
     * @return array
     */
    public function store( array $data)
    {
    }


    /**
     * @param $data
     * @param string $column
     * @return mixed
     */
    protected function workSingleImage( $data, $column = 'image')
    {
        $image = Arr::get($data, $column);

        if ($image ) {
            $uploadService = new UploadService($image);
            $uploaded = $uploadService->upload();
            $data['image'] = $uploaded;
        }
        return $data;
    }


}
