@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row append-block">
            <div class="col-12">
                @include('components.posts')

            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script defer src="{{asset('js/post-list.js')}}"></script>
@endpush
