@extends('layouts.app')

@section('content')
    <div class="col-12">


        <div class="row justify-content-between mb-4">
            <div    class="h3 col-6">    Posts edit </div>

            <div    class="col-2">
                <a href="{{route('posts.index', auth()->id())}}" class="btn btn-primary"> Back</a>

            </div>
{{--            @include('components/messages')--}}
        </div>


        <form method="POST" enctype="multipart/form-data" action="{{route('posts.update', [ auth()->id(), $post->id])}}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="exampleFormControlInput1">Name</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" name="name" value="{{$post->name}}">
            </div>

            <div class="form-group">
                <label for="exampleFormControlTextarea1">Description</label>
                <textarea class="form-control"  rows="3" name="description"> {{$post->description}} </textarea>
            </div>
            <div class="form-group">
                <label for="exampleFormControlFile1">Post image</label>
                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image" >
            </div>
            <div >
                <img src="{{$post->img_path}}" alt="" width="800">
            </div>
            <div class="form-group clearfix">

                <button type="submit" class="btn btn-primary float-right">
                    Update
                </button>
            </div>

        </form>
    </div>
@endsection
