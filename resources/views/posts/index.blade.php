@extends('layouts.app')

@section('content')
    <div class="col-12 mb-4">

        <div class="row justify-content-between">
            <div class="h3 col-6"> Posts</div>
            <div class="col-2">
                <a href="{{route('posts.create', auth()->id())}}" class="btn btn-primary">Create</a>
            </div>
            @include('components/messages')

        </div>

    </div>


    <table class="table">

        <caption>List of users</caption>
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Logo</th>
            <th scope="col">Actions</th>

        </tr>
        </thead>
        <tbody>
        @php
            $userId = auth()->id()
        @endphp
        @foreach($posts as $key =>  $post)
            <tr>
                <th scope="row">{{$key}}</th>
                <td>{{$post->name}}</td>
                <td><img src="{{$post->img_path}}" alt="" class="img-fluid" width="60px;"></td>
                <td>
                    <a href="{{route('posts.edit',[$userId, $post->id])}}" class="btn-link"> <i class="fas fa-edit"></i>
                    </a>
                    <a href="{{route('posts.show', $post->id)}}" class="btn-link ml-1"> <i class="far fa-eye"></i> </a>
                    <form action="{{route('posts.destroy',[$userId, $post->id])}}" class="d-inline-block" method="POST">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn-link ml-1" style="border: none; background: none"> <i class="fas fa-trash"></i>
                        </button>
                    </form>
{{--                    <a href="{{route('posts.destroy',[$userId, $post->id])}}" class="btn-link ml-1"> <i class="fas fa-trash"></i>--}}
                    </a>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection
