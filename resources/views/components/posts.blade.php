<div class="row content-data">


@foreach($posts as $post)
    <div class="col-md-4 ">
        <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="{{$post->img_path}}" alt="Thumbnail [100%x225]"
                 style="height: 225px; width: 100%; display: block;">
            <div class="card-body">
                <div class="card-header">
                    {{$post->name}}
                </div>
                <p class="card-text">{{$post->description}}</p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <a href="{{route('posts.show', $post->id)}}" class="btn btn-sm btn-outline-secondary">View</a>
                    </div>
                    <small class="text-muted">{{$post->updated_at->diffForHumans()}}</small>
                </div>
            </div>
        </div>
    </div>
@endforeach
</div>
@if($next = $posts->nextPageUrl())
    <div class="">
        <a class="btn btn-outline-primary load-more" href="{{$posts->nextPageUrl()}}">
            Load more...
        </a>
    </div>
@endif
