<div class="outgoing_msg">
    <div class="sent_msg">
        <p>{{$comment->text}}</p>
        <span class="time_date"> {{$comment->created_at->diffForHumans()}}</span>
    </div>
</div>
